# MachineInterlocks

A device server to manage the machine interlocks chain. This communicates with a PLC with Modbus interface, on which runs a program made by David Frichet.

## Cloning

Any instructions or special flags required to clone the project. For example, if the project has submodules in git, then specify this and give the command to chec$

```
git clone --recurse-submodules git@gitlab.esrf.fr:accelerators/SafetyAndSecurity/machineinterlocks.git
```

## Documentation

Pogo generated documentation in **doc_html** folder

## Building and Installation

### Dependencies

The project has the following dependencies.

#### Project Dependencies

* Tango Controls 9 or higher.
* omniORB release 4 or higher.
* libzmq - libzmq3-dev or libzmq5-dev.

#### Toolchain Dependencies

* C++11 compliant compiler.

### Build

Instructions on building the project.

Make example:

```bash
make
```

