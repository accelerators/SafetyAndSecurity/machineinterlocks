# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.7

# Default target executed when no arguments are given to make.
default_target: all

.PHONY : default_target

# Allow only one "make -f Makefile2" at a time, but pass parallelism.
.NOTPARALLEL:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /segfs/tango/cppserver/machine/interlocks/MachineInterlocks

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /segfs/tango/cppserver/machine/interlocks/MachineInterlocks

#=============================================================================
# Targets provided globally by CMake.

# Special rule for the target rebuild_cache
rebuild_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake to regenerate build system..."
	/usr/bin/cmake -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : rebuild_cache

# Special rule for the target rebuild_cache
rebuild_cache/fast: rebuild_cache

.PHONY : rebuild_cache/fast

# Special rule for the target edit_cache
edit_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake cache editor..."
	/usr/bin/ccmake -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : edit_cache

# Special rule for the target edit_cache
edit_cache/fast: edit_cache

.PHONY : edit_cache/fast

# The main all target
all: cmake_check_build_system
	$(CMAKE_COMMAND) -E cmake_progress_start /segfs/tango/cppserver/machine/interlocks/MachineInterlocks/CMakeFiles /segfs/tango/cppserver/machine/interlocks/MachineInterlocks/CMakeFiles/progress.marks
	$(MAKE) -f CMakeFiles/Makefile2 all
	$(CMAKE_COMMAND) -E cmake_progress_start /segfs/tango/cppserver/machine/interlocks/MachineInterlocks/CMakeFiles 0
.PHONY : all

# The main clean target
clean:
	$(MAKE) -f CMakeFiles/Makefile2 clean
.PHONY : clean

# The main clean target
clean/fast: clean

.PHONY : clean/fast

# Prepare targets for installation.
preinstall: all
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall

# Prepare targets for installation.
preinstall/fast:
	$(MAKE) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall/fast

# clear depends
depend:
	$(CMAKE_COMMAND) -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 1
.PHONY : depend

#=============================================================================
# Target rules for targets named MachineInterlocks

# Build rule for target.
MachineInterlocks: cmake_check_build_system
	$(MAKE) -f CMakeFiles/Makefile2 MachineInterlocks
.PHONY : MachineInterlocks

# fast build rule for target.
MachineInterlocks/fast:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/build
.PHONY : MachineInterlocks/fast

MachineInterlocks.o: MachineInterlocks.cpp.o

.PHONY : MachineInterlocks.o

# target to build an object file
MachineInterlocks.cpp.o:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/MachineInterlocks.cpp.o
.PHONY : MachineInterlocks.cpp.o

MachineInterlocks.i: MachineInterlocks.cpp.i

.PHONY : MachineInterlocks.i

# target to preprocess a source file
MachineInterlocks.cpp.i:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/MachineInterlocks.cpp.i
.PHONY : MachineInterlocks.cpp.i

MachineInterlocks.s: MachineInterlocks.cpp.s

.PHONY : MachineInterlocks.s

# target to generate assembly for a file
MachineInterlocks.cpp.s:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/MachineInterlocks.cpp.s
.PHONY : MachineInterlocks.cpp.s

MachineInterlocksClass.o: MachineInterlocksClass.cpp.o

.PHONY : MachineInterlocksClass.o

# target to build an object file
MachineInterlocksClass.cpp.o:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/MachineInterlocksClass.cpp.o
.PHONY : MachineInterlocksClass.cpp.o

MachineInterlocksClass.i: MachineInterlocksClass.cpp.i

.PHONY : MachineInterlocksClass.i

# target to preprocess a source file
MachineInterlocksClass.cpp.i:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/MachineInterlocksClass.cpp.i
.PHONY : MachineInterlocksClass.cpp.i

MachineInterlocksClass.s: MachineInterlocksClass.cpp.s

.PHONY : MachineInterlocksClass.s

# target to generate assembly for a file
MachineInterlocksClass.cpp.s:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/MachineInterlocksClass.cpp.s
.PHONY : MachineInterlocksClass.cpp.s

MachineInterlocksDynAttrUtils.o: MachineInterlocksDynAttrUtils.cpp.o

.PHONY : MachineInterlocksDynAttrUtils.o

# target to build an object file
MachineInterlocksDynAttrUtils.cpp.o:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/MachineInterlocksDynAttrUtils.cpp.o
.PHONY : MachineInterlocksDynAttrUtils.cpp.o

MachineInterlocksDynAttrUtils.i: MachineInterlocksDynAttrUtils.cpp.i

.PHONY : MachineInterlocksDynAttrUtils.i

# target to preprocess a source file
MachineInterlocksDynAttrUtils.cpp.i:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/MachineInterlocksDynAttrUtils.cpp.i
.PHONY : MachineInterlocksDynAttrUtils.cpp.i

MachineInterlocksDynAttrUtils.s: MachineInterlocksDynAttrUtils.cpp.s

.PHONY : MachineInterlocksDynAttrUtils.s

# target to generate assembly for a file
MachineInterlocksDynAttrUtils.cpp.s:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/MachineInterlocksDynAttrUtils.cpp.s
.PHONY : MachineInterlocksDynAttrUtils.cpp.s

MachineInterlocksStateMachine.o: MachineInterlocksStateMachine.cpp.o

.PHONY : MachineInterlocksStateMachine.o

# target to build an object file
MachineInterlocksStateMachine.cpp.o:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/MachineInterlocksStateMachine.cpp.o
.PHONY : MachineInterlocksStateMachine.cpp.o

MachineInterlocksStateMachine.i: MachineInterlocksStateMachine.cpp.i

.PHONY : MachineInterlocksStateMachine.i

# target to preprocess a source file
MachineInterlocksStateMachine.cpp.i:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/MachineInterlocksStateMachine.cpp.i
.PHONY : MachineInterlocksStateMachine.cpp.i

MachineInterlocksStateMachine.s: MachineInterlocksStateMachine.cpp.s

.PHONY : MachineInterlocksStateMachine.s

# target to generate assembly for a file
MachineInterlocksStateMachine.cpp.s:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/MachineInterlocksStateMachine.cpp.s
.PHONY : MachineInterlocksStateMachine.cpp.s

MultiClassesFactory.o: MultiClassesFactory.cpp.o

.PHONY : MultiClassesFactory.o

# target to build an object file
MultiClassesFactory.cpp.o:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/MultiClassesFactory.cpp.o
.PHONY : MultiClassesFactory.cpp.o

MultiClassesFactory.i: MultiClassesFactory.cpp.i

.PHONY : MultiClassesFactory.i

# target to preprocess a source file
MultiClassesFactory.cpp.i:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/MultiClassesFactory.cpp.i
.PHONY : MultiClassesFactory.cpp.i

MultiClassesFactory.s: MultiClassesFactory.cpp.s

.PHONY : MultiClassesFactory.s

# target to generate assembly for a file
MultiClassesFactory.cpp.s:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/MultiClassesFactory.cpp.s
.PHONY : MultiClassesFactory.cpp.s

main.o: main.cpp.o

.PHONY : main.o

# target to build an object file
main.cpp.o:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/main.cpp.o
.PHONY : main.cpp.o

main.i: main.cpp.i

.PHONY : main.i

# target to preprocess a source file
main.cpp.i:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/main.cpp.i
.PHONY : main.cpp.i

main.s: main.cpp.s

.PHONY : main.s

# target to generate assembly for a file
main.cpp.s:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/main.cpp.s
.PHONY : main.cpp.s

segfs/tango/cppserver/protocols/Modbus/src/CacheThread.o: segfs/tango/cppserver/protocols/Modbus/src/CacheThread.cpp.o

.PHONY : segfs/tango/cppserver/protocols/Modbus/src/CacheThread.o

# target to build an object file
segfs/tango/cppserver/protocols/Modbus/src/CacheThread.cpp.o:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/segfs/tango/cppserver/protocols/Modbus/src/CacheThread.cpp.o
.PHONY : segfs/tango/cppserver/protocols/Modbus/src/CacheThread.cpp.o

segfs/tango/cppserver/protocols/Modbus/src/CacheThread.i: segfs/tango/cppserver/protocols/Modbus/src/CacheThread.cpp.i

.PHONY : segfs/tango/cppserver/protocols/Modbus/src/CacheThread.i

# target to preprocess a source file
segfs/tango/cppserver/protocols/Modbus/src/CacheThread.cpp.i:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/segfs/tango/cppserver/protocols/Modbus/src/CacheThread.cpp.i
.PHONY : segfs/tango/cppserver/protocols/Modbus/src/CacheThread.cpp.i

segfs/tango/cppserver/protocols/Modbus/src/CacheThread.s: segfs/tango/cppserver/protocols/Modbus/src/CacheThread.cpp.s

.PHONY : segfs/tango/cppserver/protocols/Modbus/src/CacheThread.s

# target to generate assembly for a file
segfs/tango/cppserver/protocols/Modbus/src/CacheThread.cpp.s:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/segfs/tango/cppserver/protocols/Modbus/src/CacheThread.cpp.s
.PHONY : segfs/tango/cppserver/protocols/Modbus/src/CacheThread.cpp.s

segfs/tango/cppserver/protocols/Modbus/src/Modbus.o: segfs/tango/cppserver/protocols/Modbus/src/Modbus.cpp.o

.PHONY : segfs/tango/cppserver/protocols/Modbus/src/Modbus.o

# target to build an object file
segfs/tango/cppserver/protocols/Modbus/src/Modbus.cpp.o:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/segfs/tango/cppserver/protocols/Modbus/src/Modbus.cpp.o
.PHONY : segfs/tango/cppserver/protocols/Modbus/src/Modbus.cpp.o

segfs/tango/cppserver/protocols/Modbus/src/Modbus.i: segfs/tango/cppserver/protocols/Modbus/src/Modbus.cpp.i

.PHONY : segfs/tango/cppserver/protocols/Modbus/src/Modbus.i

# target to preprocess a source file
segfs/tango/cppserver/protocols/Modbus/src/Modbus.cpp.i:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/segfs/tango/cppserver/protocols/Modbus/src/Modbus.cpp.i
.PHONY : segfs/tango/cppserver/protocols/Modbus/src/Modbus.cpp.i

segfs/tango/cppserver/protocols/Modbus/src/Modbus.s: segfs/tango/cppserver/protocols/Modbus/src/Modbus.cpp.s

.PHONY : segfs/tango/cppserver/protocols/Modbus/src/Modbus.s

# target to generate assembly for a file
segfs/tango/cppserver/protocols/Modbus/src/Modbus.cpp.s:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/segfs/tango/cppserver/protocols/Modbus/src/Modbus.cpp.s
.PHONY : segfs/tango/cppserver/protocols/Modbus/src/Modbus.cpp.s

segfs/tango/cppserver/protocols/Modbus/src/ModbusClass.o: segfs/tango/cppserver/protocols/Modbus/src/ModbusClass.cpp.o

.PHONY : segfs/tango/cppserver/protocols/Modbus/src/ModbusClass.o

# target to build an object file
segfs/tango/cppserver/protocols/Modbus/src/ModbusClass.cpp.o:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/segfs/tango/cppserver/protocols/Modbus/src/ModbusClass.cpp.o
.PHONY : segfs/tango/cppserver/protocols/Modbus/src/ModbusClass.cpp.o

segfs/tango/cppserver/protocols/Modbus/src/ModbusClass.i: segfs/tango/cppserver/protocols/Modbus/src/ModbusClass.cpp.i

.PHONY : segfs/tango/cppserver/protocols/Modbus/src/ModbusClass.i

# target to preprocess a source file
segfs/tango/cppserver/protocols/Modbus/src/ModbusClass.cpp.i:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/segfs/tango/cppserver/protocols/Modbus/src/ModbusClass.cpp.i
.PHONY : segfs/tango/cppserver/protocols/Modbus/src/ModbusClass.cpp.i

segfs/tango/cppserver/protocols/Modbus/src/ModbusClass.s: segfs/tango/cppserver/protocols/Modbus/src/ModbusClass.cpp.s

.PHONY : segfs/tango/cppserver/protocols/Modbus/src/ModbusClass.s

# target to generate assembly for a file
segfs/tango/cppserver/protocols/Modbus/src/ModbusClass.cpp.s:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/segfs/tango/cppserver/protocols/Modbus/src/ModbusClass.cpp.s
.PHONY : segfs/tango/cppserver/protocols/Modbus/src/ModbusClass.cpp.s

segfs/tango/cppserver/protocols/Modbus/src/ModbusCore.o: segfs/tango/cppserver/protocols/Modbus/src/ModbusCore.cpp.o

.PHONY : segfs/tango/cppserver/protocols/Modbus/src/ModbusCore.o

# target to build an object file
segfs/tango/cppserver/protocols/Modbus/src/ModbusCore.cpp.o:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/segfs/tango/cppserver/protocols/Modbus/src/ModbusCore.cpp.o
.PHONY : segfs/tango/cppserver/protocols/Modbus/src/ModbusCore.cpp.o

segfs/tango/cppserver/protocols/Modbus/src/ModbusCore.i: segfs/tango/cppserver/protocols/Modbus/src/ModbusCore.cpp.i

.PHONY : segfs/tango/cppserver/protocols/Modbus/src/ModbusCore.i

# target to preprocess a source file
segfs/tango/cppserver/protocols/Modbus/src/ModbusCore.cpp.i:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/segfs/tango/cppserver/protocols/Modbus/src/ModbusCore.cpp.i
.PHONY : segfs/tango/cppserver/protocols/Modbus/src/ModbusCore.cpp.i

segfs/tango/cppserver/protocols/Modbus/src/ModbusCore.s: segfs/tango/cppserver/protocols/Modbus/src/ModbusCore.cpp.s

.PHONY : segfs/tango/cppserver/protocols/Modbus/src/ModbusCore.s

# target to generate assembly for a file
segfs/tango/cppserver/protocols/Modbus/src/ModbusCore.cpp.s:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/segfs/tango/cppserver/protocols/Modbus/src/ModbusCore.cpp.s
.PHONY : segfs/tango/cppserver/protocols/Modbus/src/ModbusCore.cpp.s

segfs/tango/cppserver/protocols/Modbus/src/ModbusStateMachine.o: segfs/tango/cppserver/protocols/Modbus/src/ModbusStateMachine.cpp.o

.PHONY : segfs/tango/cppserver/protocols/Modbus/src/ModbusStateMachine.o

# target to build an object file
segfs/tango/cppserver/protocols/Modbus/src/ModbusStateMachine.cpp.o:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/segfs/tango/cppserver/protocols/Modbus/src/ModbusStateMachine.cpp.o
.PHONY : segfs/tango/cppserver/protocols/Modbus/src/ModbusStateMachine.cpp.o

segfs/tango/cppserver/protocols/Modbus/src/ModbusStateMachine.i: segfs/tango/cppserver/protocols/Modbus/src/ModbusStateMachine.cpp.i

.PHONY : segfs/tango/cppserver/protocols/Modbus/src/ModbusStateMachine.i

# target to preprocess a source file
segfs/tango/cppserver/protocols/Modbus/src/ModbusStateMachine.cpp.i:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/segfs/tango/cppserver/protocols/Modbus/src/ModbusStateMachine.cpp.i
.PHONY : segfs/tango/cppserver/protocols/Modbus/src/ModbusStateMachine.cpp.i

segfs/tango/cppserver/protocols/Modbus/src/ModbusStateMachine.s: segfs/tango/cppserver/protocols/Modbus/src/ModbusStateMachine.cpp.s

.PHONY : segfs/tango/cppserver/protocols/Modbus/src/ModbusStateMachine.s

# target to generate assembly for a file
segfs/tango/cppserver/protocols/Modbus/src/ModbusStateMachine.cpp.s:
	$(MAKE) -f CMakeFiles/MachineInterlocks.dir/build.make CMakeFiles/MachineInterlocks.dir/segfs/tango/cppserver/protocols/Modbus/src/ModbusStateMachine.cpp.s
.PHONY : segfs/tango/cppserver/protocols/Modbus/src/ModbusStateMachine.cpp.s

# Help Target
help:
	@echo "The following are some of the valid targets for this Makefile:"
	@echo "... all (the default if no target is provided)"
	@echo "... clean"
	@echo "... depend"
	@echo "... rebuild_cache"
	@echo "... edit_cache"
	@echo "... MachineInterlocks"
	@echo "... MachineInterlocks.o"
	@echo "... MachineInterlocks.i"
	@echo "... MachineInterlocks.s"
	@echo "... MachineInterlocksClass.o"
	@echo "... MachineInterlocksClass.i"
	@echo "... MachineInterlocksClass.s"
	@echo "... MachineInterlocksDynAttrUtils.o"
	@echo "... MachineInterlocksDynAttrUtils.i"
	@echo "... MachineInterlocksDynAttrUtils.s"
	@echo "... MachineInterlocksStateMachine.o"
	@echo "... MachineInterlocksStateMachine.i"
	@echo "... MachineInterlocksStateMachine.s"
	@echo "... MultiClassesFactory.o"
	@echo "... MultiClassesFactory.i"
	@echo "... MultiClassesFactory.s"
	@echo "... main.o"
	@echo "... main.i"
	@echo "... main.s"
	@echo "... segfs/tango/cppserver/protocols/Modbus/src/CacheThread.o"
	@echo "... segfs/tango/cppserver/protocols/Modbus/src/CacheThread.i"
	@echo "... segfs/tango/cppserver/protocols/Modbus/src/CacheThread.s"
	@echo "... segfs/tango/cppserver/protocols/Modbus/src/Modbus.o"
	@echo "... segfs/tango/cppserver/protocols/Modbus/src/Modbus.i"
	@echo "... segfs/tango/cppserver/protocols/Modbus/src/Modbus.s"
	@echo "... segfs/tango/cppserver/protocols/Modbus/src/ModbusClass.o"
	@echo "... segfs/tango/cppserver/protocols/Modbus/src/ModbusClass.i"
	@echo "... segfs/tango/cppserver/protocols/Modbus/src/ModbusClass.s"
	@echo "... segfs/tango/cppserver/protocols/Modbus/src/ModbusCore.o"
	@echo "... segfs/tango/cppserver/protocols/Modbus/src/ModbusCore.i"
	@echo "... segfs/tango/cppserver/protocols/Modbus/src/ModbusCore.s"
	@echo "... segfs/tango/cppserver/protocols/Modbus/src/ModbusStateMachine.o"
	@echo "... segfs/tango/cppserver/protocols/Modbus/src/ModbusStateMachine.i"
	@echo "... segfs/tango/cppserver/protocols/Modbus/src/ModbusStateMachine.s"
.PHONY : help



#=============================================================================
# Special targets to cleanup operation of make.

# Special rule to run CMake to check the build system integrity.
# No rule that depends on this can have commands that come from listfiles
# because they might be regenerated.
cmake_check_build_system:
	$(CMAKE_COMMAND) -H$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 0
.PHONY : cmake_check_build_system

